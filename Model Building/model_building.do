/*****************************************************************
* Model Building and Variable Selection
* PBHL 8012, Spring 2016
* Adam Davey
* Requires tryem
* Type: findit tryem to install
*****************************************************************/

#delimit;
clear all;
capture log close;
log using "mymodel.log", replace;
*log using "mymodel.smcl", replace;

* Below simulates some data for the workflow;
set seed 31415;
set obs 1000;
local numx = 10;
local numxr = 5;
local evar = 5;
local maxk = 5;

forvalues i=1/`numx' {;
 generate x_`i' = rnormal();
};

gen y = 0;
forvalues i= 1/`numxr' {;
replace y = y + x_`i';
};

replace y = y + rnormal(0,`evar');

forvalues k=1/`maxk' {;
*tryem y x*, k(`k') cmd(reg) best(max) stat(r2_a);
tryem y x*, k(`k') cmd(reg) best(min) stat(rmse);
};

sw, pe(.1): reg y x*;
sw, pr(.1): reg y x*;
sw, pe(.10) pr(.100001): reg y x*;

gen order = .;
gen yhat = .;
gen fold = .;

local reps = 2;
local nfolds = 5;
quietly {;
timer on 1;
forvalues r = 1/`reps' {;
replace order = runiform();
sort order;
replace fold = mod(_n,`nfolds') + 1;
forvalues i=1/`numx' {;
 forvalues k = 1/`nfolds' {;
  tryem y x* if fold!=`k', k(`i') best(min) stat(rmse);
  predict e, resid;
  replace e = sqrt(e^2);
  summ e if fold!=`k', meanonly;
  local insamp = r(mean);
  summ e if fold==`k', meanonly;
  local outsamp = r(mean);
  *noisily: di "i = " %2.0f `i' ", fold = " %2.0f `k' ", RMSE = " %6.4f r(mean);
  mat results = (nullmat(results) \ `i', `k', `insamp', `outsamp');
  drop e;
 };
};
};
timer off 1;
svmat results;
ren results1 i;
ren results2 k;
ren results3 insamp;
ren results4 outsamp;
noisily: di "Results based on `reps' replcations";
noisily: tabstat insamp outsamp, by(i) statistics(mean) longstub nototal;
noisily: timer list 1;
by i, sort: egen insample = mean(insamp);
by i, sort: egen outsample = mean(outsamp);
twoway (line insample i, sort lwidth(thick)) (line outsample i, sort lwidth(thick)),
 ytitle(RMSE) xtitle(Number of Predictors) legend(on position(1) ring(0));
};

crossfold reg y x*, k(10) mae;

lars y x*, algorithm(lasso) graph;
