/*****************************************************************
* Sample Distributions Syntax
* PBHL 8012, Spring 2016
* Adam Davey
*****************************************************************/

#delimit;
clear all;
capture log close;
log using "mylog.log", replace;

* Below simulates some data for the workflow;
set seed 12345;
set obs 1000;

* Uniform;
gen uniform = runiform();

* Bernoulli;
gen cointoss = rbinomial(1,0.5);
di binomialp(1,1,0.5);
di binomial(1,1,0.5);
di invbinomial(1,0,0.5);

* Binomial;
gen cointosses = rbinomial(20,0.5);
di binomialp(20,10,0.5);
di binomial(20,10,0.5);
di invbinomial(20,10,0.5);
di invbinomialtail(20,10,0.5);

* Poisson;
gen helminths = rpoisson(5);
di poissonp(5,4);
di poissontail(5,5);
di poisson(5,5);


* Normal;
gen iq = rnormal(100,16);
di normalden(0);
di normalden(100,100,16);
di normal(0);
di invnormal(0.5);
di invnormal(0.975);

* Chi-squared;
gen chi10 = rchi2(10);
di chi2den(10,10);
di chi2(1,3.84);
di chi2tail(1,3.84);
di invchi2(1,0.95);
di invchi2tail(1,0.05);

* t;
gen t5 = rt(5);
di t(5,0);
di ttail(5,0);
di tden(5,0);
di invt(5,0.975);
di invttail(5,0.025);

* F;
gen f30_100 = (rchi2(30)/30)/(rchi2(100)/100);
di F(30,100,1);
di Ftail(30,100,1);
di Fden(30,100,1);
di invF(30,100,0.95);
di invFtail(30,100,0.05);

summ, detail;
log close;

* Convert text output to PDF;
translate mylog.log mylog.ps, replace;
!ps2pdf mylog.ps;
