requiredPackages <- c("car","ggplot2","gridExtra","survival","sandwich")
packagesToDownload <- requiredPackages[!(requiredPackages %in% installed.packages()[,"Package"])]
if(length(packagesToDownload) > 0){
  install.packages(packagesToDownload)
}
require(ggplot2)
require(gridExtra)
require(survival)

#image dimensions
hSize <- 720
vSize <- 360

set.seed(12345)
lambdat <- 0.2
gammat <- 2
lambdac <- 0.4
gammac <- -1

obs <- 1000
wfData <- data.frame(id <- c(1:obs))
wfData$age <- round( rnorm(obs, 50, 10) + 1)
wfData$time <- round( 10 * ((log(1 - runif(obs))) / -lambdat * exp(-0.05 * wfData$age)) ^ (1 / gammat))
wfData$censor <- round( 10 * ((log(1 - runif(obs)) / -lambdac) ^ (1 / gammac)))
wfData$died <- as.numeric(wfData$censor >= wfData$time)
wfData$time <- pmin(wfData$censor,wfData$time)
wfData$age <- 5 * round(wfData$age / 5)
wfData$old <- wfData$age >= 65

#2.1 Frequencies
wfData$time <- ifelse(wfData$time == 0,  0.01, wfData$time)
wfData$surv <- with(wfData, Surv(time, died))

ageTable <- table(wfData$age)
prop.table(ageTable)
summary(wfData$age)

#2.2 Univariate Plots
dead <- subset(wfData, died == 1)
alive <- subset(wfData, died == 0)
hisDead <- ggplot(dead, aes(x = age)) + 
  geom_histogram(binwidth = 5)
hisAlive <- ggplot(alive, aes(x = age)) + 
  geom_histogram(binwidth = 5)
grid.arrange(hisDead, hisAlive)

png(filename = "surv_boxplot.png", units = "px", width = hSize, height = vSize)
ggplot(wfData, aes(x = died, y = age)) +
  geom_boxplot()
dev.off()

png(filename = "surv_qqplot.png", units = "px", width = hSize, height = vSize)
qqnorm(wfData$age)
dev.off()

kapMeierOne <- survfit(surv ~ 1, data = wfData, conf.type = "log-log")
summary(kapMeierOne)

png(filename = "surv_kapMeier.png", units = "px", width = hSize, height = vSize)
plot(kapMeierOne)
dev.off()

#2.3

#2.4
png(filename = "surv_scatter.png", units = "px", width = hSize, height = vSize)
ggplot(wfData, aes(x=age, y=time)) +
  geom_point()
dev.off()

#2.5
png(filename = "surv_smooth1.png", units = "px", width = hSize, height = vSize)
scatter.smooth(wfData$time,wfData$died)
dev.off()

png(filename = "surv_smooth2.png", units = "px", width = hSize, height = vSize)
ggplot(wfData, aes(x=time, y=died)) +
  geom_smooth()
dev.off()

cph <- coxph(surv ~ age, data = wfData)
residuals(cph, type="martingale")

#2.6
robust <- coeftest(cph, vcov = vcovHC(mod, type="HC1"))

# 2.7 Evaluate Model
# Please see workflow text for what to look for
# Here's a margins plot
#TODO
# 2.8 Regression Diagnostics
#TODO
# 2.9 Output to excel

