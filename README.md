# Statistical Workflows #

This repository contains a collection of "workflows" for performing statistical analysis. Each folder in the repository contains a "workflow" for performing a type of analysis, as well as example code for some of the most common statistical packages.

Bill Flynn is a REDCap sorcerer