requiredPackages <- c("moments")
packagesToDownload <- requiredPackages[!(requiredPackages %in% installed.packages()[,"Package"])]
if(length(packagesToDownload) > 0){
  install.packages(packagesToDownload)
}

require(moments)
set.seed(12345)
obs <- 1000

data <- data.frame(id = c(1:obs))

data$gp2 <- c(1:obs)
data$gp3 <- c(1:obs)
data$gp2 <- as.integer(data$gp2 %% 2)
data$gp3 <- as.integer(data$gp3 %% 3)

data$x0 <- rnorm(obs)
data$x1 <- 10 * runif(obs) + 5 * data$gp2
data$x2 <- 10 * rpois(obs,5) + 3 * data$gp3

#Distributional Tests
skewness(data$x0)
kurtosis(data$x0)
shapiro.test(data$x0)

skewness(data$x1)
kurtosis(data$x1)
shapiro.test(data$x1)

skewness(data$x2)
kurtosis(data$x2)
shapiro.test(data$x2)
mean <- mean(data$x1)
sd <- sd(data$x1)
ks.test(data$x1, "pnormal", data$x1 - mean / sd) #?
ks.test(data$x1, data$gp2)#?

#Two Independent Groups (Mann-Whitney)
wilcox.test(data$x1 ~ data$gp2)

#k independent groups
kruskal.test(data$x2 ~ data$gp3)

#Sign Test
binom.test(data$x0,data$x1) #TODO Don't think this is the right test

#Sign rank test
wilcox.test(data$x1, data$x2, paired = TRUE)

#Spearman's rho
rhox0x1 <- cor.test(data$x0, data$x1, method = "spearman")
rhox0x2 <- cor.test(data$x0, data$x2, method = "spearman")
rhox1x2 <- cor.test(data$x1, data$x2, method = "spearman")
rhox0x1
rhox0x2
rhox1x2