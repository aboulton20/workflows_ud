/*****************************************************************
* Sample Nonparametric Tests Syntax
* PH 8012, Spring 2015
* Adam Davey
*****************************************************************/

#delimit;
clear all;
capture log close;
log using "mylog.log", replace;

* Below simulates some data for the workflow;
set seed 12345;
set obs 1000;

gen gp2 = mod(_n,2);
gen gp3 = mod(_n,3);

gen x0 = rnormal();
gen x1 = 10*runiform() + 5*gp2;
gen x2 = 10*rpoisson(5) + 3*gp3;

* Distributional Tests;
sktest x0 x1 x2;
swilk x0 x1 x2;
sfrancia x0 x1 x2;
summ x1;
ksmirnov x1 = normal(x1-r(mean)/r(sd));
ksmirnov x1, by(gp2);
mvtest normality x0 x1 x2, bivariate univariate stats(all);

* Two Independent Groups (Mann-Whitney);
ranksum x1, by(gp2);

* k Independent Groups;
kwallis x2, by(gp3);

* Two Dependent Groups;
* Sign test;
signtest x0=x1;

* Wilcoxon Signed-Rank Tests;
signrank x1=x2;

* Spearman's rho;
spearman x0 x1 x2, stats(rho obs p);

log close;

* Convert text output to PDF;
* Note: A two step procedure is needed under Linux;
translate mylog.log mylog.ps, replace;
!ps2pdf mylog.ps;
