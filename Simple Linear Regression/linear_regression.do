/*****************************************************************
* Sample Simple Linear Regression Syntax
* PBHL 8012, Spring 2016
* Adam Davey
* Requires outreg2
* Type: findit outreg2 to install
*****************************************************************/
#delimit;
clear all;
capture log close;
log using "mylog.log", replace;

* Below simulates some data for the workflow;
set more off;
* Uncomment line below if you are having problems with graphics;
*set graphics off;
set seed 77773;
set obs 1000;
generate age = int(20 + 60*runiform());
generate iq = int(100 + 16*rnormal() - 0.15*(age-50));

* Step 2.1 Frequencies / Summary Statistics;
tab1 iq age, missing;
summarize iq age;
* Alternatively, use syntax below for considerably more information;
*summarize iq age, detail;

* Step 2.2 Univariate Plots;
hist iq;
graph export "hist_iq.pdf", replace;
graph box iq;
graph export "box_iq.pdf", replace;
qnorm iq;
graph export "qnorm_iq.pdf", replace;

hist age;
graph export "hist_age.pdf", replace;
graph box age;
graph export "box_age.pdf", replace;
qnorm age;
graph export "qnorm_age.pdf", replace;

* Step 2.3 Testing Normality of Distributions;
sktest iq;
sktest age;

* Step 2.4 Scatterplot;
scatter iq age, sort;
graph export "scatter_iq_age.pdf", replace;

* Step 2.5 Lowess;
lowess iq age, sort;
graph export "lowess_iq_age.pdf", replace;

* Step 2.6 Estimate Model;
* First, estimate and store OLS model;
regress iq age;
estimates store ols;

* Then estimate and store model with robust SEs;
regress iq age, vce(robust);
estimates store robust;

* Step 2.7 Evaluate Model;
* No additional analyses or syntax for this section, so here's a handy figure;
graph twoway (lfitci iq age) (scatter iq age);
graph export "twoway_fitted_cis.pdf", replace;

* Step 2.8 Regression Diagnostics;
* Things we can do with robust standard errors;
* First ensure robust standard error model is active;
estimates restore robust;
* Predicted values;
predict iqhat, xb;
* Predicted residuals;
predict iqres, resid;
hist iqres, normal;
graph export "hist_iqres.pdf", replace;
swilk iqres;
* Hat test;
linktest;
qnorm iqres;
graph export "qnorm_iqres.pdf", replace;
szroeter age;
rvfplot, yline(0);
graph export "rvfplot.pdf", replace;
rvpplot age, yline(0);
graph export "rvpplot_age.pdf", replace;

* Things we can do with OLS standard errors;
* First restore OLS estimates;
estimates restore ols;
estat hettest;
dfbeta age;
lvr2plot;
graph export "lvr2plot.pdf", replace;

* Step 2.9 Tabling Results;
* NEVER type numbers you don't need to;
* Let the computer do the heavy lifting for you;
* Example 1 -- direct to Excel;
* Crap table, but all the information is where you put it;
quietly: estimates replay robust;
putexcel set "regression1.xls", sheet("Robust SEs") replace;
putexcel F1=("Number of obs") G1=(e(N));
putexcel F2=("F")             G2=(e(F));
putexcel F3=("Prob > F")      G3=(Ftail(e(df_m), e(df_r), e(F)));
putexcel F4=("R-squared")     G4=(e(r2));
putexcel F5=("Adj R-squared") G5=(e(r2_a));
putexcel F6=("Root MSE")      G6=(e(rmse));
matrix a = r(table)';
matrix a = a[.,1..6];
putexcel A8=matrix(a, names);
quietly: estimates replay ols;
putexcel set "regression1.xls", sheet("OLS SEs") modify;
putexcel F1=("Number of obs") G1=(e(N));
putexcel F2=("F")             G2=(e(F));
putexcel F3=("Prob > F")      G3=(Ftail(e(df_m), e(df_r), e(F)));
putexcel F4=("R-squared")     G4=(e(r2));
putexcel F5=("Adj R-squared") G5=(e(r2_a));
putexcel F6=("Root MSE")      G6=(e(rmse));
matrix a = r(table)';
matrix a = a[.,1..6];
putexcel A8=matrix(a, names);
* Example 2 -- also direct to Excel;
* Nicer table, but labels need to be changed;
outreg2 [robust ols] using "regression2",
 replace excel stats(coef ci pval)
 bdec(2) cdec(2) pdec(3) bracket(ci) noaster sideway;

log close;

* Convert text output to PDF;
translate myreglog.log myreglog.ps, replace;
!ps2pdf myreglog.ps;
